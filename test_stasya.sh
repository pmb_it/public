#!/bin/bash
TEST=(10.0.1.1 93.183.200.35 8.8.8.8)

echo "" > ~/Downloads/test.log

echo "$(date)" >> ~/Downloads/test.log

ifconfig >> ~/Downloads/test.log

/System/Library/PrivateFrameworks/Apple80211.framework/Resources/airport -I >> ~/Downloads/test.log

for IP in ${TEST[*]}
do
        ping -c 10 -i 0.2 $IP >> ~/Downloads/test.log
done


echo "Output: ~/Downloads/test.log"
